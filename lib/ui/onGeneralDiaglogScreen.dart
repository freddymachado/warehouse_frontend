import 'package:empoorio_screens/helpers/helpers.dart';
import 'package:empoorio_screens/ui/widgets/EmpoorioModalBottomSheet.dart';
import 'package:empoorio_screens/ui/widgets/appButton.dart';
import 'package:empoorio_screens/ui/widgets/appText.dart';
import 'package:empoorio_screens/ui/widgets/bottomBox.dart';
import 'package:empoorio_screens/ui/widgets/customCard.dart';
import 'package:empoorio_screens/ui/widgets/dialogAppBar.dart';
import 'package:empoorio_screens/ui/widgets/optionBoxItem.dart';
import 'package:flutter/material.dart';

class onGeneralDialogScreen extends StatefulWidget {
  onGeneralDialogScreen({Key? key}) : super(key: key);

  @override
  State<onGeneralDialogScreen> createState() => _onGeneralDialogScreenState();
}

class _onGeneralDialogScreenState extends State<onGeneralDialogScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DialogAppBar(title: 'DialogAppBar'),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: AppText(
                        text: 'CustomCard',
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GridView(
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisExtent: 130,
                            mainAxisSpacing: 8,
                            crossAxisSpacing: 8),
                        children: [
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '1',
                          ),
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '5',
                          ),
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '7',
                          ),
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '10',
                          ),
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '100',
                          ),
                          CustomCard(
                            text: 'text',
                            image: 'assets/images/box_all.png',
                            number: '1000',
                          )
                        ]),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: AppText(
                        text: 'CustomCard',
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GridView(
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3, mainAxisSpacing: 5, crossAxisSpacing: 5),
                        children: [
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          ),
                          OptionBoxItem(
                            image: 'assets/images/box_all.png',
                            text: 'Text',
                          )
                        ]),
                        SizedBox(height: 100,)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      bottomSheet: BottomBoxWidget(
        widget: AppButton(
          onTap: () {
            empoorioModalBottomSheet(
                context,
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AppText(text: 'Do you want to close this view?'),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: AppButton(
                              onTap: () {},
                              title: 'Cancel',
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: AppButton(
                              titleColor: Colors.white,
                              backgroundColor: Colors.red,
                              onTap: () {
                                Navigator.pop(context);
                                Navigator.pop(context);
                              },
                              title: 'Accept',
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ));
          },
          title: 'Close',
        ),
      ),
    );
  }
}
