import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class CustomCardImage extends StatelessWidget {
  final double? width;
  final double? height;
  final dynamic image;
  final bool? up;
  final bool? m2;
  final bool? m3;

  Function()? onTap;

  void Function()? onDelete;

  CustomCardImage({
    Key? key,
    this.width,
    this.height,
    this.up,
    this.m2,
    this.m3,
    this.onTap,
    this.onDelete,
    this.image = 'assets/images/box_all.png',
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(2),
        width: width,
        height: height,
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 6,
                offset: Offset(1, 1),
              ),
            ],
            image: image is File
                ? DecorationImage(image: FileImage(image), fit: BoxFit.cover, opacity: 0.8)
                : DecorationImage(image: NetworkImage(image), fit: BoxFit.cover, opacity: 0.8)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              onTap: onDelete,
              child: Container(
                child: Icon(
                  CupertinoIcons.clear_circled,
                  color: Colors.red,
                  size: 30,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
