import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'appText.dart';


// ignore: must_be_immutable
class OrDivider extends StatelessWidget {
  String text;
  Color? color;
  OrDivider({Key? key, this.text = 'O', this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: Divider(
            color: color ?? (isDarkMode(context) ? Colors.white : Colors.black),
          )),
          SizedBox(
            width: 10,
          ),
          AppText(
            text: text,
            textColor: color ?? (isDarkMode(context) ? Colors.white : Colors.black),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Divider(
            color: color ?? (isDarkMode(context) ? Colors.white : Colors.black),
          )),
        ],
      ),
    );
  }
}
