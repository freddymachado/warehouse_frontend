import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';

class LoadWidget extends StatelessWidget {
  final double size;
  final Color? loadingColor;
  const LoadWidget({super.key, this.loadingColor, this.size = 21});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: size,
        width: size,
        child: CircularProgressIndicator(
          strokeWidth: 1,
          valueColor: AlwaysStoppedAnimation(
              loadingColor ?? (isDarkMode(context) ? Colors.black : Colors.white)),
        ));
  }
}
