import 'dart:io';

import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'appText.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? titleText;
  final List<Widget>? actions;
  final Widget? imagePath;
  final Size preferredAppBarSize;
  final Color? backgroundColor; // Nuevo argumento para el color de fondo
  void Function()? onBack;
  double textFontSize;
  Widget? onBackIcon;
  bool activeLeading;

  CustomAppBar({
    Key? key,
    this.titleText,
    this.actions,
    this.imagePath,
    this.onBack,
    this.textFontSize = 14,
    this.activeLeading = true,
    Size? preferredAppBarSize,
    this.backgroundColor, // Agregar el color de fondo requerido
  })  : preferredAppBarSize = preferredAppBarSize ?? const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  Size get preferredSize => preferredAppBarSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: backgroundColor, // Usar el color de fondo proporcionado
      titleSpacing: 0,
      automaticallyImplyLeading: false,
      iconTheme: IconThemeData(
        color: !isDarkMode(context) ? Colors.black : Colors.white,
      ),
      leading: activeLeading == false
          ? null
          : GestureDetector(
              onTap: onBack ??
                  () {
                    Navigator.pop(context);
                  },
              child: onBackIcon != null
                  ? onBackIcon
                  : Platform.isAndroid
                      ? Icon(
                          Icons.arrow_back,
                        )
                      : Icon(Icons.arrow_back_ios_new_rounded),
            ),
      title: Padding(
        padding: EdgeInsets.only(left: activeLeading == false ? 10 : 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (imagePath != null) imagePath!,
            const SizedBox(width: 8),
            titleText != null
                ? Expanded(
                    child: AppText(
                    text: titleText!,
                    fontSize: textFontSize,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                  ))
                : SizedBox(),
            if (actions != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: const EdgeInsets.only(right: 20),
                    child: Row(
                      children: actions ?? [],
                    ),
                  )
                ],
              ),
          ],
        ),
      ),
    );
  }
}
