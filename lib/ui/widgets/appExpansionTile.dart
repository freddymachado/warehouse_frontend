import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';

class AppExpansionTile extends StatelessWidget {
  final Widget? leading;
  final Widget title;
  final Widget? subtitle;
  final List<Widget> children;
  final Widget? trailing;

  const AppExpansionTile(
      {super.key,
      this.leading,
      required this.title,
      this.subtitle,
      this.children = const [],
      this.trailing});

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      tilePadding: EdgeInsets.zero,
      childrenPadding: EdgeInsets.zero,
      iconColor: !isDarkMode(context) ? Colors.black : Colors.white,
      collapsedTextColor: !isDarkMode(context) ? Colors.black : Colors.white,
      textColor: !isDarkMode(context) ? Colors.black : Colors.white,
      leading: leading,
      trailing: trailing,
      title: title,
      subtitle: subtitle,
      expandedAlignment: Alignment.topLeft,
      children: children,
    );
  }
}
