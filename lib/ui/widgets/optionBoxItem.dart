import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'appText.dart';

class OptionBoxItem extends StatelessWidget {
  String text;
  double fontSize;
  String image;
  void Function()? onTap;
  bool selected;
  OptionBoxItem(
      {super.key,
      this.text = '',
      this.fontSize = 14,
      this.onTap,
      this.selected = false,
      this.image = 'assets/images/box_all.png'});

  @override
  Widget build(BuildContext context) {
    double size = MediaQuery.of(context).size.width * 0.29;
    return InkWell(
      onTap: onTap,
      child: Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
            color: selected
                ? !isDarkMode(context)
                    ? Colors.black
                    : Colors.white
                : isDarkMode(context)
                    ? Colors.black
                    : Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 6,
                offset: Offset(1, 1), // changes position of shadow
              ),
            ]),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                image,
                height: 35,
              ),
              SizedBox(
                height: 5,
              ),
              AppText(
                textColor: selected
                    ? isDarkMode(context)
                        ? Colors.black
                        : Colors.white
                    : !isDarkMode(context)
                        ? Colors.black
                        : Colors.white,
                text: text,
                textAlign: TextAlign.center,
                fontSize: fontSize,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
