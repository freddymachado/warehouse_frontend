import 'package:flutter/material.dart';
import '../../helpers/helpers.dart';
import 'loadWidget.dart';

class AppButton extends StatelessWidget {
  String title;
  Color? backgroundColor;
  Color? titleColor;
  Color? borderColor;
  bool enabled;
  Function onTap;
  var icon;
  double verticalPadding;
  double? fontSize;
  Widget? widget;
  Color? iconColor;
  FontWeight? fontWeight;
  bool loading;
  Color? loadingColor;

  AppButton(
      {Key? key,
      this.title = '',
      this.backgroundColor,
      this.titleColor,
      this.borderColor,
      this.enabled = true,
      this.icon,
      this.verticalPadding = 12.0,
      this.fontSize,
      this.widget,
      this.iconColor,
      this.fontWeight,
      this.loading = false,
      this.loadingColor,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minWidth: double.infinity),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          padding: EdgeInsets.only(top: verticalPadding, bottom: verticalPadding),
          backgroundColor: enabled == false
              ? backgroundColor != null
                  ? backgroundColor!.withOpacity(0.8)
                  : !isDarkMode(context)
                      ? Colors.black.withOpacity(0.8)
                      : Colors.white.withOpacity(0.8)
              : backgroundColor != null
                  ? backgroundColor
                  : !isDarkMode(context)
                      ? Colors.black
                      : Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: BorderSide(
                  color: borderColor ?? Colors.transparent, width: borderColor != null ? 1 : 0)),
        ),
        onPressed: () {
          if (enabled) {
            onTap();
          }
        },
        child: loading
            ? LoadWidget(loadingColor: loadingColor)
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (icon != null)
                    Icon(
                      icon!,
                      color: iconColor ?? (isDarkMode(context) ? Colors.black : Colors.white),
                      size: 18,
                    ),
                  if (icon != null)
                    SizedBox(
                      width: 6,
                    ),
                  if (widget != null) widget!,
                  if (widget != null)
                    SizedBox(
                      width: 6,
                    ),
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: fontWeight,
                      fontFamily: 'Poppinsl',
                      fontSize: fontSize,
                      color: enabled == false
                          ? titleColor != null
                              ? titleColor!.withOpacity(0.6)
                              : isDarkMode(context)
                                  ? Colors.black.withOpacity(0.6)
                                  : Colors.white.withOpacity(0.6)
                          : titleColor != null
                              ? titleColor
                              : isDarkMode(context)
                                  ? Colors.black
                                  : Colors.white,
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
