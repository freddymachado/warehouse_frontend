import 'package:flutter/material.dart';

import 'appText.dart';

class DialogAppBar extends StatelessWidget {
  final String title;
  const DialogAppBar({
    super.key,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      child: Column(
        children: [
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(Icons.clear)),
                Expanded(
                  child: AppText(
                    text: title,
                    textAlign: TextAlign.center,
                    fontSize: 14,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Icon(
                  Icons.clear,
                  color: Colors.transparent,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
