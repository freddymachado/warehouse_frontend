import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'appExpansionTile.dart';
import 'appText.dart';

class CustomExpansionTile extends StatelessWidget {
  final List<Widget> customChildren;
  final String title;
  final String firstSubtitle;
  final String? subtitle;
  final String image;
  FontWeight? titleFontWeight;

  CustomExpansionTile(
      {Key? key,
      this.customChildren = const [],
      required this.title,
      this.firstSubtitle = '',
      this.subtitle = '',
      this.titleFontWeight,
      this.image = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppExpansionTile(
      title: Row(
        children: [
          if (image != '')
            Container(
              margin: EdgeInsets.only(right: 20),
              child: Image.asset(
                image,
                width: 25,
                color: !isDarkMode(context) ? Colors.black : Colors.white,
              ),
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText(text: title),
              if (firstSubtitle != '')
                AppText(
                  text: firstSubtitle,
                  fontSize: 12,
                  fontWeight: titleFontWeight,
                ),
            ],
          ),
          Spacer(),
          AppText(
            text: subtitle!,
            fontSize: 12,
          ),
        ],
      ),
      children: customChildren,
    );
  }
}
