import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../helpers/helpers.dart';


class AppTextFormField extends StatelessWidget {
  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function(String?)? onSubmitted;
  TextEditingController? controller;
  List<TextInputFormatter>? inputFormatters;

  String? initialValue;
  int? minLines;
  int? maxLines;
  String? hintText;
  TextInputType? keyboardType;
  String? title;
  TextInputAction? textInputAction;
  TextCapitalization? textCapitalization;
  bool readOnly;
  Widget? prefix;
  bool? enabled;
  Widget? suffix;
  bool obscureText;
  Widget? suffixIcon;
  Widget? prefixIcon;
  EdgeInsetsGeometry? customContentPadding;
  void Function(String)? onFieldSubmitted;
  void Function()? onTap;
  int? maxLength;
  AppTextFormField({
    Key? key,
    this.validator,
    this.onChanged,
    this.onSaved,
    this.controller,
    this.initialValue,
    this.minLines,
    this.hintText,
    this.keyboardType,
    this.title,
    this.textInputAction,
    this.textCapitalization,
    this.readOnly = false,
    this.prefix,
    this.enabled,
    this.onFieldSubmitted,
    this.suffix,
    this.suffixIcon,
    this.obscureText = false,
    this.prefixIcon,
    this.maxLines,
    this.onTap,
    this.customContentPadding,
    this.maxLength,
    this.onSubmitted,
    this.inputFormatters,
  }) : super(key: key);

  BorderRadius borderRadius = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (this.title != '' && this.title != null)
          Text(
            title!,
            style: TextStyle(
              fontFamily: 'Poppinsl',
              color: isDarkMode(context) ? Colors.white : Colors.black,
            ),
          ),
        if (this.title != '' && this.title != null)
          SizedBox(
            height: 5,
          ),
        TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            maxLength: maxLength,
            inputFormatters: inputFormatters,
            onTap: onTap,
            obscureText: obscureText,
            onFieldSubmitted: onFieldSubmitted,
            enabled: enabled,
            readOnly: readOnly,
            minLines: this.minLines ?? 1,
            maxLines: this.maxLines ?? 1,
            controller: this.controller,
            initialValue: initialValue,
            textAlign: TextAlign.start,
            textInputAction: textInputAction,
            keyboardType: keyboardType,
            textCapitalization: textCapitalization ?? TextCapitalization.none,
            style: TextStyle(fontFamily: 'Poppinsl', fontSize: 16.0),
            cursorColor: !isDarkMode(context) ? Colors.black : Colors.white,
            onSaved: onSaved,
            onChanged: onChanged,
            validator: validator,
            decoration: InputDecoration(
              errorStyle: TextStyle(
                fontFamily: 'Poppinsl',
                fontSize: 12,
              ),
              errorMaxLines: 2,
              prefixIcon: prefixIcon,
              suffixIcon: suffixIcon,
              suffix: suffix,
              prefix: prefix,
              contentPadding:
                  customContentPadding ?? EdgeInsets.only(left: 10, right: 10, top: 4, bottom: 4),
              hintText: this.hintText ?? '',
              focusedBorder: OutlineInputBorder(
                  borderRadius: borderRadius,
                  borderSide: BorderSide(
                      color: !isDarkMode(context) ? Colors.black : Colors.white, width: 2.0)),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).errorColor),
                borderRadius: borderRadius,
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).errorColor),
                borderRadius: borderRadius,
              ),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: isDarkMode(context) ? Colors.grey.shade800 : Colors.grey.shade200),
                borderRadius: borderRadius,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey.shade400),
                borderRadius: borderRadius,
              ),
            )),
      ],
    );
  }
}
