import 'package:flutter/material.dart';
import '../../helpers/helpers.dart';
import 'appText.dart';

customSnackBar(BuildContext context, String text) {
  return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    duration: Duration(milliseconds: 1500),
    padding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
    behavior: SnackBarBehavior.fixed,
    content: AppText(
      text: text,
    ),
    backgroundColor: !isDarkMode(context) ? Colors.black : Colors.white,
  ));
}
