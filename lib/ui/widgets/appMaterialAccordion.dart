import 'package:empoorio_screens/ui/widgets/typeOfServiceItem.dart';
import 'package:flutter/material.dart';

import 'appExpansionTile.dart';
import 'appText.dart';

class AppMaterialAccordion extends StatelessWidget {
  final dynamic material;

  const AppMaterialAccordion({
    super.key,
    required this.material,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: AppExpansionTile(
      title: AppText(text: material.name),
      children: [
        // Agrega la imagen aquí
        const SizedBox(height: 10),

        Column(
          children: [
            AppText(
              text: material.description,
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
                height: 30,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: material.whereToBuy.length,
                  itemBuilder: (context, index) {
                    return TypeOfServiceItem(
                      title: material.whereToBuy[index]['linkName'],
                      onTap: () {
                        print(
                            'https://www.alibaba.com/product-detail/Direct-thermal-label-35mm-x-25mm_60658992548.html?fromMSite=true');
                      },
                    );
                  },
                )),
          ],
        ),
        const SizedBox(height: 10),
      ],
    ));
  }
}
