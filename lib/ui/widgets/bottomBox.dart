import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';

class BottomBoxWidget extends StatelessWidget {
  Widget? widget;
  BottomBoxWidget({Key? key, this.widget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 30, top: 10, left: 20, right: 20),
      color: isDarkMode(context) ? Colors.black : Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          widget ?? const SizedBox(),
        ],
      ),
    );
  }
}
