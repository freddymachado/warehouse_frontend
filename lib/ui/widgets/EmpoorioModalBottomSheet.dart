import 'dart:io';

import 'package:flutter/material.dart';
import '../../helpers/helpers.dart';

Future<dynamic> empoorioModalBottomSheet(BuildContext context, Widget body,
    [String title = '', bool isScrollControlled = false, bool isDismissible = true]) {
  return showModalBottomSheet(
    isDismissible: isDismissible,
    isScrollControlled: true,
    backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: SizedBox(
              width: double.infinity,
              child: Column(
              
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 6,
                    width: 60,
                    decoration: BoxDecoration(
                        color: !isDarkMode(context) ? Colors.black : Colors.white,
                        borderRadius: BorderRadius.circular(30)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  if (title != '')
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'Poppinsl',
                                fontSize: 16,
                                color: isDarkMode(context) ? Colors.white : Colors.black),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: 10,
                        maxHeight: isScrollControlled
                            ? MediaQuery.of(context).size.height * 0.75
                            : MediaQuery.of(context).size.height / 2.6),
                    child: body,
                  ),
                  Platform.isIOS
                      ? SizedBox(
                          height: 30,
                        )
                      : SizedBox(
                          height: 20,
                        )
                ],
              ),
            ),
          );
        },
      );
    },
  );
}
