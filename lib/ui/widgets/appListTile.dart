import 'package:flutter/material.dart';

import 'appText.dart';


// ignore: must_be_immutable
class AppListTile extends StatelessWidget {
  String title;
  Widget? leading;
  Widget? subtitle;
  Widget? trailing;
  Function()? onTap;
  EdgeInsetsGeometry? contentPadding;
  ListTileTitleAlignment? titleAlignment;
  Widget? widget;
  double? horizontalTitleGap;
  double titleFontSize;
  AppListTile(
      {super.key,
      this.title = '',
      this.leading,
      this.trailing,
      this.onTap,
      this.subtitle,
      this.contentPadding,
      this.titleAlignment,
      this.horizontalTitleGap,
      this.titleFontSize = 16,
      this.widget});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      titleAlignment: titleAlignment,
      contentPadding: contentPadding,
      onTap: onTap,
      title: title != ''
          ? AppText(
              text: title,
              fontSize: titleFontSize,
            )
          : widget != null
              ? widget
              : SizedBox(),
      leading: leading,
      subtitle: subtitle,
      trailing: trailing,
      horizontalTitleGap: horizontalTitleGap,
    );
  }
}
