import 'package:flutter/material.dart';

class AppText extends StatelessWidget {
  final String text;
  final String? subtitle;
  final TextOverflow? overflow;
  final TextAlign? textAlign;
  final TextStyle textStyle;
  final double fontSize;
  final FontWeight? fontWeight;
  final Color? textColor;
  final String? fontFamily;
  final int? maxLines;

  const AppText(
      {super.key,
      required this.text,
      this.overflow,
      this.textAlign = TextAlign.left,
      TextStyle? style,
      this.fontSize = 14,
      this.fontWeight,
      this.textColor,
      this.fontFamily,
      this.subtitle,
      this.maxLines})
      : textStyle = style ?? const TextStyle();

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,
      style: textStyle.copyWith(
        color: textColor,
        fontFamily: fontFamily ?? 'Poppinsl',
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
    );
  }
}
