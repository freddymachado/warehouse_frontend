import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'EmpoorioModalBottomSheet.dart';
import 'appTextFormField.dart';

class AppSelect extends StatelessWidget {
  String? Function(String?)? validator;
  TextEditingController? controller;
  String? hintText;
  bool enabled;
  String title;
  Widget? widget;
  int maxLines;
  double cover;
  bool isScrollControlled;
  bool onlyTitleInModal;

  AppSelect(
      {Key? key,
      required this.controller,
      this.hintText,
      this.widget,
      this.maxLines = 1,
      this.cover = 1.0,
      this.title = '',
      this.onlyTitleInModal = false,
      this.isScrollControlled = true,
      this.validator,
      this.enabled = true})
      : super(key: key);

  BorderRadius borderRadius = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return AppTextFormField(
        validator: validator,
        readOnly: true,
        title: onlyTitleInModal ? '' : title,
        hintText: hintText,
        controller: controller,
        suffixIcon: Icon(
          Icons.arrow_drop_down,
          color: !isDarkMode(context) ? Colors.black : Colors.white,
        ),
        onTap: () {
          if (enabled) {
            empoorioModalBottomSheet(context, StatefulBuilder(
              builder: (BuildContext context, setState) {
                return widget != null
                    ? widget!
                    : SizedBox(
                        height: 100,
                      );
              },
            ), title, isScrollControlled);
          }
        });
  }
}
