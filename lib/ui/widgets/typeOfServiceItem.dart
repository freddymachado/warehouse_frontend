import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';


class TypeOfServiceItem extends StatelessWidget {
  void Function()? onTap;
  bool active;
  String title;
  double? width;
  Widget? prefix;
  double rightMargin;
  FontWeight? fontWeight;
  double fontSize ;
  TypeOfServiceItem(
      {Key? key,
      this.onTap,
      this.active = true,
      this.title = '',
      this.width,
      this.prefix,
      this.rightMargin = 5,
      this.fontSize = 14,
      this.fontWeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width,
        margin: EdgeInsets.only(right: rightMargin),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: active
                ? isDarkMode(context)
                    ? Colors.grey.shade900
                    : Colors.grey.shade200
                : isDarkMode(context)
                    ? Colors.white
                    : Colors.black),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (prefix != null)
              Row(
                children: [
                  prefix!,
                  SizedBox(
                    width: 5,
                  )
                ],
              ),
            Text(
              title,
              style: TextStyle(
                  fontWeight: fontWeight,
                  fontFamily: 'Poppinsl',
                  fontSize: fontSize ,
                  color: active
                      ? isDarkMode(context)
                          ? Colors.white
                          : Colors.black
                      : !isDarkMode(context)
                          ? Colors.white
                          : Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
