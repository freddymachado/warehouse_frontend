import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';
import 'appText.dart';


class CustomCard extends StatelessWidget {
  final String text;
  final double? width;
  final double? height;
  final String number;
  final String image;
  final bool? up;
  final bool? m2;
  final bool? m3;
  bool selected;
  EdgeInsetsGeometry? margin;

  Function()? onTap;
  CustomCard(
      {super.key,
      required this.text,
      this.width,
      this.height,
      this.number = '4',
      this.up,
      this.m2,
      this.m3,
      this.onTap,
      this.margin,
      this.selected = false,
      this.image = 'assets/images/box_all.png'});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: margin,
        width: width,
        height: height,
        decoration: BoxDecoration(
            color: selected
                ? !isDarkMode(context)
                    ? Colors.black
                    : Colors.white
                : isDarkMode(context)
                    ? Colors.black
                    : Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 1,
                blurRadius: 6,
                offset: Offset(1, 1), // changes position of shadow
              ),
            ]),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    image,
                    width: 45,
                    height: 45,
                  ),
                  if (up == null && m2 == null && m3 == null)
                    Expanded(
                      child: AppText(
                        textAlign: TextAlign.end,
                        text: number.toString(),
                        fontSize: number.length > 8 ? 11 : 20,
                        fontWeight: FontWeight.bold,
                        textColor: selected
                            ? isDarkMode(context)
                                ? Colors.black
                                : Colors.white
                            : !isDarkMode(context)
                                ? Colors.black
                                : Colors.white,
                      ),
                    ),
                  if (m2 != null)
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        AppText(
                          text: number.toString(),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          textColor: selected
                              ? isDarkMode(context)
                                  ? Colors.black
                                  : Colors.white
                              : !isDarkMode(context)
                                  ? Colors.black
                                  : Colors.white,
                        ),
                        Column(
                          children: [
                            SizedBox(
                              height: 5,
                            ),
                            AppText(
                              text: 'm2',
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              textColor: selected
                                  ? isDarkMode(context)
                                      ? Colors.black
                                      : Colors.white
                                  : !isDarkMode(context)
                                      ? Colors.black
                                      : Colors.white,
                            ),
                          ],
                        ),
                      ],
                    ),
                  if (m3 != null)
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        AppText(
                          text: number.toString(),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          textColor: selected
                              ? isDarkMode(context)
                                  ? Colors.black
                                  : Colors.white
                              : !isDarkMode(context)
                                  ? Colors.black
                                  : Colors.white,
                        ),
                        Column(
                          children: [
                            SizedBox(
                              height: 5,
                            ),
                            AppText(
                              text: 'm3',
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              textColor: selected
                                  ? isDarkMode(context)
                                      ? Colors.black
                                      : Colors.white
                                  : !isDarkMode(context)
                                      ? Colors.black
                                      : Colors.white,
                            ),
                          ],
                        ),
                      ],
                    ),
                  if (up != null)
                    Stack(
                      children: [
                        Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                AppText(
                                  text: number.toString(),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  textColor: selected
                                      ? isDarkMode(context)
                                          ? Colors.black
                                          : Colors.white
                                      : !isDarkMode(context)
                                          ? Colors.black
                                          : Colors.white,
                                ),
                                SizedBox(
                                  width: 10,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        ),
                        up!
                            ? Positioned(
                                right: 0,
                                bottom: 0,
                                child: Image.asset(
                                  'assets/images/grafico_sube.png',
                                  height: 16,
                                ),
                              )
                            : Positioned(
                                right: 0,
                                bottom: 0,
                                child: Image.asset(
                                  'assets/images/grafico_baja.png',
                                  height: 16,
                                ),
                              )
                      ],
                    ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: AppText(
                  text: text,
                  fontSize: text.length > 30 ? 10 : 12,
                  textColor: selected
                      ? !isDarkMode(context)
                          ? Colors.white
                          : Colors.black
                      : isDarkMode(context)
                          ? Colors.white
                          : Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
