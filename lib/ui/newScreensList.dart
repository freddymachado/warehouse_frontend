import 'package:empoorio_screens/helpers/helpers.dart';
import 'package:empoorio_screens/ui/onGeneralDiaglogScreen.dart';
import 'package:empoorio_screens/ui/widgets/appButton.dart';
import 'package:empoorio_screens/ui/widgets/appExpansionTile.dart';
import 'package:empoorio_screens/ui/widgets/appListTile.dart';
import 'package:empoorio_screens/ui/widgets/appSelect.dart';
import 'package:empoorio_screens/ui/widgets/appText.dart';
import 'package:empoorio_screens/ui/widgets/appTextFormField.dart';
import 'package:empoorio_screens/ui/widgets/bottomBox.dart';
import 'package:empoorio_screens/ui/widgets/customAppBar.dart';
import 'package:empoorio_screens/ui/widgets/orDivider.dart';
import 'package:empoorio_screens/ui/widgets/typeOfServiceItem.dart';
import 'package:flutter/material.dart';

class NewScreensList extends StatefulWidget {
  NewScreensList({Key? key}) : super(key: key);

  @override
  State<NewScreensList> createState() => _NewScreensListState();
}

class _NewScreensListState extends State<NewScreensList> {
  TextEditingController controller = TextEditingController();
  List numbers = [1, 2, 3];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
      appBar: CustomAppBar(
        backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
        titleText: 'widget example',
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              AppText(
                text: 'These are some widgets we have created (The text also has a widget)',
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
              SizedBox(
                height: 20,
              ),
              AppTextFormField(
                title: 'AppTextFormField',
              ),
              SizedBox(
                height: 20,
              ),
              AppSelect(
                  controller: controller,
                  title: 'AppSelect',
                  widget: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return AppListTile(
                        onTap: () {
                          controller.text = index.toString();
                          setState(() {});
                        },
                        title: index.toString(),
                        trailing: Icon(Icons.arrow_right),
                      );
                    },
                  )),
              SizedBox(
                height: 20,
              ),
              AppExpansionTile(
                title: AppText(
                  text: 'Magna eiusmod duis ad adipisicing',
                ),
                children: [
                  AppText(
                      text:
                          'Sint cupidatat magna esse minim proident adipisicing. Laborum anim eu quis proident officia voluptate amet velit dolor officia do. Officia duis ea do eu culpa id ipsum excepteur ea non commodo cupidatat. In deserunt labore in nostrud. Aliqua commodo tempor exercitation et enim anim id velit deserunt. Sunt do ex proident enim ullamco aute id. Cillum duis eiusmod sit sunt officia dolor veniam mollit qui.'),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 30,
                        child: TypeOfServiceItem(
                          title: 'Edit',
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: TypeOfServiceItem(
                          rightMargin: 0,
                          title: 'Delete',
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
              AppListTile(
                contentPadding: EdgeInsets.zero,
                titleFontSize: 12,
                title: 'Reprehenderit dolor',
                trailing: Icon(Icons.arrow_right),
              ),
              SizedBox(
                  width: 200,
                  child: OrDivider(
                    text: 'Or',
                    color: Colors.grey[500],
                  )),
              AppListTile(
                contentPadding: EdgeInsets.zero,
                titleFontSize: 12,
                title: 'Reprehenderit dolor occaecat ',
                trailing: Icon(Icons.arrow_right),
              ),
              SizedBox(
                  width: 200,
                  child: OrDivider(
                    text: 'Or',
                    color: Colors.grey[500],
                  )),
              AppListTile(
                contentPadding: EdgeInsets.zero,
                titleFontSize: 12,
                title: 'Reprehenderit dolor occaecat veniam velit tempor.',
                trailing: Icon(Icons.arrow_right),
              ),
              SizedBox(
                height: 80,
              )
            ],
          ),
        ),
      ),
      bottomSheet: BottomBoxWidget(
        widget: AppButton(
          onTap: () {
            showGeneralDialog(
              context: context,
              pageBuilder: (context, animation, secondaryAnimation) {
                return onGeneralDialogScreen();
              },
            );
          },
          title: 'Button',
        ),
      ),
    );
  }
}
