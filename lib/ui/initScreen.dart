import 'package:empoorio_screens/helpers/helpers.dart';
import 'package:empoorio_screens/ui/newScreensList.dart';
import 'package:empoorio_screens/ui/widgets/appButton.dart';
import 'package:empoorio_screens/ui/widgets/bottomBox.dart';
import 'package:empoorio_screens/ui/widgets/customAppBar.dart';
import 'package:flutter/material.dart';

class InitScreen extends StatefulWidget {
  InitScreen({Key? key}) : super(key: key);

  @override
  State<InitScreen> createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
      appBar: CustomAppBar(
        backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
        titleText: 'Home',
        activeLeading: false,
      ),
      bottomSheet: BottomBoxWidget(
          widget: AppButton(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewScreensList(),
              ));
        },
        title: 'Example',
      )),
    );
  }
}
